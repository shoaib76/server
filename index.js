const express = require('express');
const mangooes = require('mongoose');
const http = require("http");
const https = require("https");
const fs = require('fs');


const DB = "mongodb+srv://munir:munir123@cluster0.dsddxlf.mongodb.net/?retryWrites=true&w=majority"

const authRouter = require("./routes/auth");

const app = express();
const privateKey = fs.readFileSync('selfsigned.key', 'utf8');
const certificate = fs.readFileSync('selfsigned.crt', 'utf8');
const credentials = { key: privateKey, cert: certificate };
// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Methods", "GET,PUT,PATCH,POST,DELETE");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });
app.use(express.json());
app.use(authRouter);


mangooes
    .connect(DB)
    .then(() => {
        console.log("Connection Successful")
    })
    .catch((e) => {
        console.log(e);
    })

//let httpapp = http.createServer(app);
let httpsapp = https.createServer(credentials, app);

httpsapp.listen(3000, "0.0.0.0", () => {

    console.log('Connected 3000 ');
});

//httpapp.listen(3001, "0.0.0.0", () => {

  //  console.log('Connected 3000 ');
//});
