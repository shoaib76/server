const express = require('express');
const User = require('../model/user');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const authRouter = express.Router();

authRouter.get("/test", (req, res) => {
  res.send("<h1>It's working 🤗</h1>")
})

authRouter.post("/api/signup", async (req, res) => {
  try {
    const { name, email ,password } = req.body;
    console.log("signup request cimming")
 
    const exeUser = await User.findOne({ email });
  
    if (exeUser) {
      console.log("jjjj");
      return res
        .status(400)
        .json({ msg: "User with same email already exists!" });
    }

    const hashedPassword = await bcrypt.hash(password, 8);

    let user = new User({
      email,
      name,
      password: hashedPassword,
    });
    user = await user.save();
    res.json(user);
  } catch (e) {
    res.status(500).json({ error: e.message });
  }

});

authRouter.post("/api/signin", async (req, res) => {
  try {
    const { email, password } = req.body;
console.log("signin request cimming")
    const user = await User.findOne({ email });
    
    if (!user) {
      return res
        .status(400)
        .json({ msg: "User with this email does not exist!" });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({ msg: "Incorrect password." });
    }
    
    const token = jwt.sign({ id: user._id }, "passwordKey");
    console.log("Login successful")
    res.json({ token, ...user._doc });
    
  } catch (e) {
    res.status(500).json({ error: e.message });
  }
});

module.exports = authRouter;
